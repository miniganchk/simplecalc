// КОД ОБРАБОТКИ НАЖАТИЙ НА КНОПКИ

//Собрать элементы страницы для управления ими
const calcButton = document.getElementById("calcButton");
const clearButton = document.getElementById("clearButton");
const param1Input = document.getElementById("param1");
const param2Input = document.getElementById("param2");
const operationSelect = document.getElementById("operation");
const resultInput = document.getElementById("result");


/**
 * Рассчиать и вывести результат
 */
function calculate() {
    //Прочитать данные из полей ввода
    const param1 = Number(param1Input.value);
    const param2 = Number(param2Input.value);
    const operation = operationSelect.value;

    //Переменная для записи результата
    let result = '';

    //Определить тип операции и вычислить результат
    switch (operation) {
        case '+':
            result = param1 + param2;
            break;
        case '-':
            result = param1 - param2;
            break;
        //Вычисление модуля числа Param1
        case 'abs':
            result = Math.abs(param1);
	        break;
	    //вычисление логарифма от param1
    	case 'ln':
            result = Math.log(param1);
            break;
        case '*':
            result = param1 * param2;
            break;
        case '/':
            result = param1 / param2;
            break;
    }

    //Вывести результат
    resultInput.value = result;
}

/**
 *
 */
function clearFields() {
    //Очистить параметр 1
    param1Input.value = '';
    //Очистить параметр 2
    param2Input.value = '';
    //Очистить поле результата
    resultInput.value = '';
}

//Подключить обработчики нажатия кнопок
calcButton.addEventListener('click', calculate);
clearButton.addEventListener('click', clearFields);